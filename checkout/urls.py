# coding=utf-8

from django.conf.urls import url

from checkout.views import OrderListView
from .views import CreateCartItemView, CartItemView, CheckoutView

urlpatterns = [
    url(
        r'^carrinhol/adicionar/(?P<slug>[\w_-]+)/$',
        CreateCartItemView.as_view(), name='create_cartitem'),

    url(r'^carrinho/$', CartItemView.as_view(), name='cart_item'),

    url(r'^finalizando/$', CheckoutView.as_view(), name='checkout'),

    url(r'^meus_pediso/$', OrderListView.as_view(), name='order_list'),
]
