from django.contrib import admin

from .models import Category, Product


class CategoryAdmin(admin.ModelAdmin):
    '''
        Admin com definições para a Classe Category
    '''
    list_display = ['name', 'slug', 'created', 'modified']
    search_fields = ['name', 'slug']
    list_filter = ['created', 'modified']


class ProductAdmin(admin.ModelAdmin):
    '''
        Admin da classe product
    '''
    list_display = ['name', 'slug', 'category', 'created', 'modified']
    search_fields = ['name', 'slug', 'category__name']
    list_filter = ['created', 'modified']


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
